#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define CONS 536870912

int main(){
	//Lo siguiente es para que el valor de rand() varie y no nos entregue siempre lo mismo
	srand(time(NULL));

	//Definicion de variables
	long int *arreglo = malloc(CONS*sizeof(long int*));
	int random,i;
	clock_t start, end;
	double cpu_time_used;

	//Se inicia el conteo del tiempo
	start = clock();
	
	//Recorremos el arreglo de manera aleatoria, para probar la localidad temporal
	for (i = 0; i < CONS; ++i)
	{
		//Creamos un valor al azar
		random = rand()%CONS;
		//Asignamos a la posicion al azar, el valor 0
		arreglo[random] = 0;
	}

	//Se detiene el conteo del tiempo
	end = clock();

	//Se obtiene el tiempo utilizado por la aplicacion
	cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;

	printf("Con localidad espacial, el programa se ha demorado %f segundos.\n",cpu_time_used);

	free(arreglo);
	return 0;
}