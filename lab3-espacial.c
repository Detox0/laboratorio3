#include <stdio.h>
#include <stdlib.h>
#include <time.h>

//Constante necesaria para el programa
#define CONS 536870912

int main(){
	//Lo siguiente es para que el valor de rand() varie y no nos entregue siempre lo mismo
	srand(time(NULL));

	//Definicion de variables
	long int *arreglo = malloc(CONS*sizeof(long int*));
	int random,i;
	clock_t start, end;
	double cpu_time_used;

	//Inicio del conteo del tiempo
	start = clock();
	
	//Recorremos el arreglo de 1 en 1 para probar la localidad espacial
	for (i = 0; i < CONS; ++i)
	{
		//Creamos un valor al azar
		random = rand();
		//Asignamos el valor aleatorio al arreglo
		arreglo[i] = random;
	}

	//Fin del conteo del tiempo
	end = clock();

	//Se calcula el tiempo ocupado por la aplicacion
	cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;

	printf("Con localidad espacial, el programa se ha demorado %f segundos.\n",cpu_time_used);

	return 0;
}